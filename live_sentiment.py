import pathlib
from scipy.spatial import distance as dist
import numpy as np
import cv2
from imutils import face_utils
from imutils.video import VideoStream
from fastai.vision import *
import imutils
import argparse
import time
import dlib

HOME_DIR = pathlib.Path(__file__).parent.absolute()

L_START, L_END = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
R_START, R_END = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

EYE_AR_THRESH = 0.20
EYE_AR_CONSEC_FRAMES = 10


class FaceDetect:
    vs = VideoStream(src=0).start()
    start = time.perf_counter()
    model = load_learner(HOME_DIR, 'export.pkl')
    face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    shape_predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    counter = 0
    time_value = 0

    def __init__(self, show_video=True, video_writer=None, output_data_file=None):
        self.show_video = show_video
        self.video_writer = video_writer
        self.output_data_file = output_data_file

    def eye_aspect_ratio(self, eye):
        A = dist.euclidean(eye[1], eye[5])
        B = dist.euclidean(eye[2], eye[4])
        C = dist.euclidean(eye[0], eye[3])
        eye_aspect_ratio = (A + B) / (2.0 * C)
        return eye_aspect_ratio

    def get_data(self, prediction, probability, eye_aspect_ratio_score):
        current_time = int(time.perf_counter() - self.start)
        data = None
        if current_time != self.time_value:
            data = [current_time, prediction, probability, eye_aspect_ratio_score]
            self.time_value = current_time
        return data

    def process_frame(self, frame):
        H, W, _ = frame.shape

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        face_coord = self.face_cascade.detectMultiScale(gray, 1.1, 5, minSize=(30, 30))

        data = None
        for coords in face_coord:
            X, Y, w, h = coords
            X_1, X_2 = (max(0, X - int(w * 0.3)), min(X + int(1.3 * w), W))
            Y_1, Y_2 = (max(0, Y - int(0.3 * h)), min(Y + int(1.3 * h), H))
            img_cp = gray[Y_1:Y_2, X_1:X_2].copy()
            prediction, idx, probability = self.model.predict(Image(pil2tensor(img_cp, np.float32).div_(225)))

            cv2.rectangle(
                    img=frame,
                    pt1=(X_1, Y_1),
                    pt2=(X_2, Y_2),
                    color=(128, 128, 0),
                    thickness=2,
                )
            rect = dlib.rectangle(X, Y, X+w, Y+h)

            cv2.putText(frame, str(prediction), (10, frame.shape[0] - 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (225, 255, 255), 2)

            shape = self.shape_predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)
            left_eye = shape[L_START:L_END]
            right_eye = shape[R_START:R_END]
            left_eye_aspect_ratio = self.eye_aspect_ratio(left_eye)
            right_eye_aspect_ratio = self.eye_aspect_ratio(right_eye)
            eye_aspect_ratio_score = (left_eye_aspect_ratio + right_eye_aspect_ratio) / 2.0
            left_eye_hull = cv2.convexHull(left_eye)
            right_eye_hull = cv2.convexHull(right_eye)
            cv2.drawContours(frame, [left_eye_hull], -1, (0, 255, 0), 1)
            cv2.drawContours(frame, [right_eye_hull], -1, (0, 255, 0), 1)

            data = self.get_data(prediction, probability, eye_aspect_ratio_score)
        return data, frame

    def process(self):
        all_data = []
        while True:
            frame = self.vs.read()
            frame = imutils.resize(frame, width=450)
            data, out_frame = self.process_frame(frame)
            if data is not None and self.output_data_file is not None:
                all_data.append(data)
            if self.video_writer is not None:
                self.video_writer.write(out_frame)
            if self.show_video:
                cv2.imshow("frame", out_frame)

            if cv2.waitKey(1) & 0xFF == ord("q"):
                break
        self.vs.stop()
        return all_data


def main():
    args = vars(parser.parse_args())

    video_writer = None
    if args["save"]:
        video_writer = cv2.VideoWriter(HOME_DIR + "liveoutput.avi", cv2.VideoWriter_fourcc('M','J','P','G'), 10, (450,253))

    data = []
    face_detect = FaceDetect(video_writer=video_writer, output_data_file="data")
    face_detect.process()

    if args["savedata"]:
        df = pd.DataFrame(data, columns = ['Time (seconds)', 'Expression', 'Probability', 'EAR'])
        df.to_csv(HOME_DIR+'/exportlive.csv')
        print("Data exported to exportlive.csv")

    if args["save"]:
        print("Finished saving video")
        video_writer.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save", dest="save", action="store_true")
    parser.add_argument("--save-data", dest="savedata", action="store_true")
    main()
