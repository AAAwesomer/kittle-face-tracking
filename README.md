# Kittle Face Tracking
Use a deep learning model to predict facial expressions from a videostream

## Running locally

### Python version
Python version used is 3.7.9. Does not work for python versions 3.8+.

### Install requirements
`pip install -r requirements.txt`

### Detect facial expression from live video

Run the code<br/>

`python live_sentiment.py`<br/>

Press 'q' to exit.

Additional tags:<br/>
--save to save video with predictions and landmarking <br/>
--save-data to save csv file with expression predictions, their probability tensor and eye aspect ratio
